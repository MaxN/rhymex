import tensorflow as tf
from tensorflow import keras
import numpy as np
import os
from rhymex.utils import load_data, save_tokenizer, load_tokenizer, prepare_input
from rhymex.hyperparams import SEQ_LENGTH, NUM_CHARS, MODEL_OUTPUT_PATH
print(tf.__version__)

if not os.path.exists(MODEL_OUTPUT_PATH):
    os.makedirs(MODEL_OUTPUT_PATH)



print('reading train_stress.txt')
train_x, train_y = load_data(os.path.join('tmp', 'train_stress.txt'), maxlen=SEQ_LENGTH - 1)
train_x2, train_y2 = load_data('manual_stress.txt', maxlen=SEQ_LENGTH - 1)
train_x = train_x + train_x2
train_y = np.concatenate((train_y, train_y2))

# import pdb; pdb.set_trace()
print('loaded {} lines'.format(len(train_x)))
print('reading valid_stress.txt')
valid_x, valid_y = load_data(os.path.join('tmp', 'valid_stress.txt'),maxlen=SEQ_LENGTH - 1)
print('loaded {} lines'.format(len(valid_x)))

token = keras.preprocessing.text.Tokenizer(num_words=NUM_CHARS, char_level=True)
print('fit with tokenizer')
token.fit_on_texts(train_x)

save_tokenizer(token, os.path.join(MODEL_OUTPUT_PATH, 'tokenizer.bin'))

print('encode chars into integer and pads them')
train_seq_x = prepare_input(train_x, token, SEQ_LENGTH)
valid_seq_x = prepare_input(valid_x, token, SEQ_LENGTH)

# print('y to categorical ')
# valid_cat_y = tf.keras.utils.to_categorical(valid_y, num_classes=SEQ_LENGTH, dtype='int16')
# train_cat_y = tf.keras.utils.to_categorical(train_y, num_classes=SEQ_LENGTH, dtype='int16')

# print(train_seq_x.shape)
# print(train_seq_x[1])
model = keras.models.Sequential([
  keras.layers.Embedding(NUM_CHARS, 128, input_length=SEQ_LENGTH),
  keras.layers.LSTM(128),
  keras.layers.Dense(SEQ_LENGTH, activation='softmax')
  ])
model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
model.summary()


# if os.path.exists(os.path.join(MODEL_OUTPUT_PATH, 'weights.h5')):
  # model.load_weights(os.path.join(MODEL_OUTPUT_PATH, 'weights.h5'))
# else:
model.fit(train_seq_x, train_y, batch_size=64, epochs=10, validation_data=(valid_seq_x, valid_y), verbose=1)
model.save_weights(os.path.join(MODEL_OUTPUT_PATH, 'weights.h5'))
model.save(os.path.join(MODEL_OUTPUT_PATH, 'model.h5'))