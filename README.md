rhymex 
======
This is small python library for NLP tasks. It predicts the stress of word and can divide word for syllables. Also it predicts whether words are rhyme or not. Initially it's designed for russian language, but you can easily redesign it for any language.

Install
=======

```
pip install rhymex
```

Usage
=====

```python
from rhymex.stress_model import StressModel
from rhymex.syllables import get_syllables
from rhymex import is_rhyme
from rhymex.word_profile import WordProfile

syllables = get_syllables('банка')
syllables[0].text # => бан
syllables[1].text # => ка

is_rhyme('чувак', 'мудак') # => True
is_rhyme('в', 'любовь') # => False

stress_model = StressModel()
stress_model.predict('майка') # => 2
stress_model.predict('колесом') # => 6
stress_model.predict('я') # => 1


```

Training
========

Unrar  `vocabs/all_form.rar` file. It russian dictionary with stresses. Then start:

```
python3 preprocess.py
```

It will create two files for training and validation: `tmp/train_stress.txt`, `tmp/valid_stress.txt`. The format of these files is very simple:

```
<word>\t<stress_index>
```
where stress_index starts from 1

Now you can train the model

```
python3 train.py
```

After train you can run test

```
python3 test.py
```