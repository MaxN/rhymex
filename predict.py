import os
from rhymex.stress_model import StressModel
import sys
from rhymex.hyperparams import MODEL_OUTPUT_PATH

model = StressModel(MODEL_OUTPUT_PATH)


if len(sys.argv) == 1:
  print('please enter word')
  exit()

print("Input word {}".format(sys.argv[1]))
print(model.predict(sys.argv[1]))
