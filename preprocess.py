import os
import random 
from os import path

def preprocess_file(fn, output_path, input_encoding='cp1251'):
  if not path.exists(output_path):
    os.makedirs(output_path)

  with open(fn, 'r', encoding=input_encoding) as f:
    with open(path.join(output_path, 'train_stress.txt'), 'w') as train_f:
      with open(path.join(output_path, 'valid_stress.txt'), 'w') as valid_f:
        
        for l in f.readlines():
          word, forms = l.split('#')
          words = forms.split(',')
          if random.randint(0, 100) > 30:
            output_f = train_f
          else:
            output_f = valid_f
          for w in words:
            try:

              pos = w.index('\'')
              w = w.replace('\'', '').replace('`', '').strip()
              if w == '':
                continue

              # import pdb; pdb.set_trace()
              output_line = "{}\t{}\n".format(w, pos)
              output_f.write(output_line)
            except:
              pass

if __name__ == "__main__":
  preprocess_file('vocabs/All_Forms.txt', 'tmp')