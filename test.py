import unittest

from rhymex.stress_model import StressModel
from rhymex.hyperparams import MODEL_OUTPUT_PATH
from rhymex.syllables import get_syllables
from rhymex import is_rhyme
from rhymex.word_profile import WordProfile

class TestMain(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.stress_model = StressModel()


    def test_stress(self):
        return
        checks = {
            'я': 1,
            'в': 0,
            'он': 1,
            'украина': 4,
            'майка': 2,
            'соломка': 4,
            'изжить': 4,
            'виться': 2,
            'данный': 2,
            'зорька': 2,
            'банка': 2,
            'оттечь': 4,
            'пора': 4,
            'меда': 2,
            'советского': 4,
            'союза': 3,
            'изжила': 6,
            'автоподъёмник': 9,
            'каракуля': 4,
            'супервайзер': 7,
            'колесом': 6
        }
        for word, pos in checks.items():
            predicted = self.stress_model.predict(word)
            target = pos
            self.assertEqual(predicted, target, msg="{}: {} vs {}".format(word, predicted, target))

    def test_syllables(self):
        l = get_syllables('банка')
        self.assertEqual(len(l), 2)
        self.assertEqual(l[0].text, 'бан')
        self.assertEqual(l[1].text, 'ка')

    def test_get_stressed_syllable(self):
        p = WordProfile('грустишь')
        self.assertEqual(2, p.syllables_count)
        self.assertEqual(1, p.stressed_syllable_index)

    def test_is_rhymex(self):
        self.assertTrue(is_rhyme('братишь', 'грустишь'))
        self.assertTrue(is_rhyme('заебал', 'проебал'))
        self.assertTrue(is_rhyme('чувак', 'мудак'))
        
    def test_is_rhymex(self):
        self.assertFalse(is_rhyme('в', 'любовь'))

if __name__ == '__main__':
    unittest.main()